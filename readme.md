Studentnaam: Jorn Palsenbarg

Studentnummer: 568662

---
# Algemene beschrijving applicatie
Proxy applicatie met 

##  Ontwerp en bouw de *architectuur* van de applicatie die HTTP-requests van een willekeurige PC opvangt en doorstuurt naar één webserver. (Teken een diagram en licht de onderdelen toe)


##  Zorg voor een voorbeeld van een http-request en van een http-response.
(Kan je globale overeenkomsten vinden tussen een request en een response?)  (Teken een diagram en licht de onderdelen toe)


##  TCP/IP
###  Beschrijving van concept in eigen woorden
###  Code voorbeeld van je eigen code
###  Alternatieven & adviezen
###  Authentieke en gezaghebbende bronnen


##  Bestudeer de RFC van HTTP 1.1.
###  Hoe ziet de globale opbouw van een HTTP bericht er uit? (Teken een diagram en licht de onderdelen toe)
###  Uit welke componenten bestaan een HTTP bericht.  (Teken een diagram en licht de onderdelen toe)
###  Hoe wordt de content in een bericht verpakt? (Teken een diagram en licht de onderdelen toe)
###  Streaming content

##  Kritische reflectie op eigen werk (optioneel, maar wel voor een 10)
###  Wat kan er beter? Geef aan waarom?
###  Wat zou je een volgende keer anders doen?
###  Hoe zou de opdracht anders moeten zijn om er meer van te leren?

# Test cases

### Case naam
### Case handeling
### Case verwacht gedrag

# Kritische reflectie op eigen beroepsproduct

### Definieer kwaliteit in je architectuur, design, implementatie.
### Geef voorbeelden.
### Wat kan er beter, waarom?
