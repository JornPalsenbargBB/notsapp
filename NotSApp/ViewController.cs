﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;

using System.Text;

using AppKit;
using Foundation;

namespace NotSApp
{
	
	public partial class ViewController : NSViewController
	{
        ProxySettings proxySettings = new ProxySettings();
        ProxyServer proxy;

        bool proxyRunning = false;


		public ViewController(IntPtr handle) : base(handle)
		{
		}

		partial void ListenButton(AppKit.NSButton sender)
		{
			try
			{
                if (!proxyRunning) {
                    UIupdateAfterStart();
                    proxySettings.BufferSize = Int32.Parse(BufferSize.StringValue);
                    proxy.Start(proxySettings);
                } else {
                    proxy.Stop();
                    UIupdateAfterStop();
                }
				
                proxyRunning = !proxyRunning;

                //chatServer = new ChatServer(uiUpdater);
                //chatServer.setBufferSize(Int32.Parse(BufferSize.StringValue));
				//chatServer.startServer(e => {
				//	updateMessageBox("Something went wrong while setting up the server\n" + e.Message);
				//	enableFields();
				//});

			}
			catch (Exception e) 
			{
				updateMessageBox("Something went wrong while setting up the server\n" + e.Message);
                UIupdateAfterStop();
			}
		}

        partial void CacheToggler(AppKit.NSButton sender)
        {
            proxySettings.CachingEnabled = !proxySettings.CachingEnabled;
        }

        partial void ImageFilterToggler(AppKit.NSButton sender)
        {
            proxySettings.ImageFilterEnabled = !proxySettings.ImageFilterEnabled;
        }

        partial void IdentityProtectionToggler(AppKit.NSButton sender)
        {
            proxySettings.IdentityProtectionEnabled = !proxySettings.IdentityProtectionEnabled;
        }

        partial void AuthenticationToggler(AppKit.NSButton sender)
        {
            proxySettings.AuthenticationEnabled = !proxySettings.AuthenticationEnabled;
        }

        partial void StreamingToggler(AppKit.NSButton sender) {
            proxySettings.StreamingEnabled = !proxySettings.StreamingEnabled;
        }

		public void updateMessageBox(string message)
		{
			MessageViewer.InvokeOnMainThread(() => {
				MessageViewer.Value += message + "\n";
			});
		}

		private void UIupdateAfterStart()
		{
			ListenButtonOutlet.InvokeOnMainThread(() => {
                ListenButtonOutlet.Title = "Stop";
			});
			BufferSize.InvokeOnMainThread(() => {
				BufferSize.Editable = false;
			});
		}

		private void UIupdateAfterStop() 
		{
			ListenButtonOutlet.InvokeOnMainThread(() => {
				ListenButtonOutlet.Title = "Start";
			});
			BufferSize.InvokeOnMainThread(() => {
				BufferSize.Editable = true;
			});
		}

		public override void ViewDidLoad()
		{
			base.AwakeFromNib();
			BufferSize.StringValue = "1024";

            proxy = new ProxyServer(updateMessageBox, (e) => {
                updateMessageBox("Error: " + e.Message);
            });
		}

		public override NSObject RepresentedObject
		{
			get
			{
				return base.RepresentedObject;
			}
			set
			{
				base.RepresentedObject = value;
				// Update the view, if already loaded.
			}
		}
	}
}
