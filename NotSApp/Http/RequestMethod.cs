﻿using System;
namespace NotSApp
{
	public class RequestMethod
	{
		private RequestMethod(string value) { Value = value; }

		public string Value { get; set; }

		public static RequestMethod GET { get { return new RequestMethod("GET"); } }
		public static RequestMethod HEAD { get { return new RequestMethod("HEAD"); } }
		public static RequestMethod POST { get { return new RequestMethod("POST"); } }
		public static RequestMethod PUT { get { return new RequestMethod("PUT"); } }
		public static RequestMethod DELETE { get { return new RequestMethod("DELETE"); } }
		public static RequestMethod CONNECT { get { return new RequestMethod("CONNECT"); } }
		public static RequestMethod OPTIONS { get { return new RequestMethod("OPTIONS"); } }
		public static RequestMethod TRACE { get { return new RequestMethod("TRACE"); } }

        /// <summary>
        /// Returns new RequestMethod
        /// </summary>
        /// <returns>RequestMethod</returns>
        /// <param name="Method">RequestMethod as string</param>
		public static RequestMethod GetNew(string Method)
		{
			switch (Method.ToUpper())
			{
				case "GET":
					return RequestMethod.GET;
				case "HEAD":
					return RequestMethod.HEAD;
				case "POST":
					return RequestMethod.POST;
				case "PUT":
					return RequestMethod.PUT;
				case "DELETE":
					return RequestMethod.DELETE;
				case "CONNECT":
					return RequestMethod.CONNECT;
				case "OPTIONS":
					return RequestMethod.OPTIONS;
				case "TRACE":
					return RequestMethod.TRACE;
				default:
					throw new ArgumentOutOfRangeException("Invalid RequestMethod");
			}
		}

        public override string ToString() {
            return Value;
        }
	}
}
