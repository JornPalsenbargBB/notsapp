﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NotSApp
{
	public abstract class HttpMessage
	{
		protected Dictionary<string, string> Headers = new Dictionary<string, string>();
		protected Byte[] Body = new Byte[0];
		public string HttpVersion = "";

        /// <summary>
        /// Returns start line from httpmessage.
        /// </summary>
        /// <returns>The start line string.</returns>
		public abstract string GetStartLineString();

        /// <summary>
        /// Appends to body.
        /// </summary>
        /// <param name="Bytes">Bytes.</param>
		public void AppendToBody(Byte[] Bytes)
		{
			int BodyOriginalLength = Body.Length;
			Array.Resize<Byte>(ref Body, Body.Length + Bytes.Length);
			Array.Copy(Bytes, 0, Body, BodyOriginalLength, Bytes.Length);
		}

        /// <summary>
        /// Checks if message has header
        /// </summary>
        /// <returns><c>true</c>, if message has header, <c>false</c> if not.</returns>
        /// <param name="key">Key.</param>
        public bool HasHeader(string key) {
            return Headers.ContainsKey(key.ToLower());
        }

        /// <summary>
        /// Gets header.
        /// </summary>
        /// <returns>The header.</returns>
        /// <param name="key">Key.</param>
		public string GetHeader(string key)
		{
            return Headers[key.ToLower()];
		}

        /// <summary>
        /// Sets header.
        /// </summary>
        /// <param name="key">Key.</param>
        /// <param name="value">Value.</param>
		public void SetHeader(string key, string value)
		{
            Headers[key.ToLower()] = value;
		}

        /// <summary>
        /// Removes header.
        /// </summary>
        /// <param name="key">Key.</param>
        public void removeHeader(string key) {
            try
            {
                Headers.Remove(key.ToLower());
            } catch(Exception){}
        }

        /// <summary>
        /// Get message as Byte Array
        /// </summary>
        /// <returns>The byte array.</returns>
		public Byte[] GetByteArray()
		{
			Byte[] Message = Encoding.ASCII.GetBytes(GetHeadString());
			int MessageOriginalLength = Message.Length;
			Array.Resize<Byte>(ref Message, Message.Length + Body.Length);
			Array.Copy(Body, 0, Message, MessageOriginalLength, Body.Length);
			return Message;
		}

        /// <summary>
        /// Gets a string with start line and all headers.
        /// </summary>
        /// <returns>The head string.</returns>
		public string GetHeadString()
		{
			string HttpHead = GetStartLineString() + "\r\n";
			foreach (KeyValuePair<string, string> Header in Headers)
			{
				HttpHead += Header.Key + ": " + Header.Value + "\r\n";
			}
			HttpHead += "\r\n";
			return HttpHead;
		}
	}
}
