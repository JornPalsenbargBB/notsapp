﻿using System;
namespace NotSApp
{
	public class ResponseStatus
	{
		private ResponseStatus(string statusCode, string reasonPhrase) {
			StatusCode = statusCode;
			ReasonPhrase = reasonPhrase;
		}

		public string StatusCode { get; set; }
		public string ReasonPhrase { get; set; }



	}
}
