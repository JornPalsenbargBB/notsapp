﻿using System;
namespace NotSApp
{
	public class HttpRequestMessage : HttpMessage
	{
		public RequestMethod RequestMethod = null;
		public string RequestUri = "";

		public HttpRequestMessage()
		{
		}

		public override string GetStartLineString()
		{
			return RequestMethod.Value + " " + RequestUri + " " + HttpVersion;
		}
	}
}
