﻿using System;
using System.Text;

namespace NotSApp
{
	public class HttpParser
	{
		public HttpParser()
		{
		}

        /// <summary>
        /// Parses Byte Array to Http Request
        /// </summary>
        /// <returns>HttpRequestMessage</returns>
        /// <param name="ByteRequest">Byte request.</param>
        public HttpRequestMessage ByteArrayToHttpRequest(Byte[] ByteRequest)
        {
            HttpRequestMessage HttpRequest = new HttpRequestMessage();

            Boolean StartLineEnd = false;
            int index = 0;
            string Message = "";
            // Lees start line
            while (!StartLineEnd)
            {
                char Character = Convert.ToChar(ByteRequest[index]);

                switch (Character)
                {
                    case '\n':
                        HttpRequest.HttpVersion = Message;
                        StartLineEnd = true;
                        break;
                    case ' ':
                        if (HttpRequest.RequestMethod == null)
                        {
                            HttpRequest.RequestMethod = RequestMethod.GetNew(Message);
                        }
                        else
                        {
                            HttpRequest.RequestUri = Message;
                        }
                        Message = "";
                        break;
                    case '\r':
                        break;
                    default:
                        Message += Character;
                        break;
                }

                index++;
            }

            HttpRequest = (HttpRequestMessage)ParseHeaders(HttpRequest, ByteRequest, ref index);

            if (index < ByteRequest.Length) {
                Byte[] body = new Byte[ByteRequest.Length - index];
                Array.Copy(ByteRequest, index, body, 0, ByteRequest.Length - index);
                HttpRequest.AppendToBody(body);
            }

			return HttpRequest;
		}

        /// <summary>
        /// Parses Byte Array to Http Response
        /// </summary>
        /// <returns>HttpResponseMessage</returns>
        /// <param name="ByteResponse">Byte response.</param>
		public HttpResponseMessage ByteArrayToHttpResponse(Byte[] ByteResponse)
		{
			HttpResponseMessage HttpResponse = new HttpResponseMessage();

            Boolean StartLineEnd = false;
            int index = 0;
            string Message = "";
            // Lees start line
            while (!StartLineEnd)
            {
                char Character = Convert.ToChar(ByteResponse[index]);

                switch (Character)
                {
                    case '\n':
                        HttpResponse.ReasonPhrase = Message;
                        StartLineEnd = true;
                        break;
                    case ' ':
                        if (HttpResponse.HttpVersion == "")
                        {
                            //HttpRequest.RequestMethod = RequestMethod.GetNew(Message);
                            HttpResponse.HttpVersion = Message;
                            Message = "";
                        } else if (HttpResponse.StatusCode == "")
                        {
                            HttpResponse.StatusCode = Message;
                            Message = "";
                        } else {
                            Message += Character;
                        }
                        break;
                    case '\r':
                        break;
                    default:
                        Message += Character;
                        break;
                }

                index++;
            }

            HttpResponse = (HttpResponseMessage)ParseHeaders(HttpResponse, ByteResponse, ref index);

            if (index < ByteResponse.Length)
            {
                Byte[] body = new Byte[ByteResponse.Length - index];
                Array.Copy(ByteResponse, index, body, 0, ByteResponse.Length - index);
                HttpResponse.AppendToBody(body);
            }

			return HttpResponse;
		}

        private HttpMessage ParseHeaders(HttpMessage HttpMessage, Byte[] ByteRequest, ref int index)
		{
			Boolean EndOfHeaders = false;
            Boolean headerSeperator = false;
			string Message = "";
			string Key = "";

			while (!EndOfHeaders)
			{
				char Character = Convert.ToChar(ByteRequest[index]);

				switch (Character)
				{
					case '\n':
                        if ( (Convert.ToChar(ByteRequest[index - 1]) == '\n') 
                            || (Convert.ToChar(ByteRequest[index - 1]) == '\r' && Convert.ToChar(ByteRequest[index - 2]) == '\n')) {
							EndOfHeaders = true;
							break;
                        }
                        HttpMessage.SetHeader(Key, Message);
						Key = "";
						Message = "";
                        headerSeperator = false;
						break;
					case ':':
                        if (!headerSeperator) {
                            Key = Message;
                            Message = "";
                            headerSeperator = true;
                        } else {
                            
                        }
						break;
					case ' ':
                        if (!(Convert.ToChar(ByteRequest[index - 1]) == ':')) {
                            Message += Character;
                        }
						break;
                    case '\r':
                        break;
					default:
						Message += Character;
						break;
				}

				index++;
			}

            return HttpMessage;
		}
	}
}
