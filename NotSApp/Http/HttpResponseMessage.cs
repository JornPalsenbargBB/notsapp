﻿using System;
namespace NotSApp
{
	public class HttpResponseMessage : HttpMessage
	{

		public string StatusCode = "";
		public string ReasonPhrase = "";

		public override string GetStartLineString()
		{
			return HttpVersion + " " + StatusCode + " " + ReasonPhrase;
		}
	}
}
