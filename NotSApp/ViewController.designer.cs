// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace NotSApp
{
	[Register ("ViewController")]
	partial class ViewController
	{
		[Outlet]
		AppKit.NSButton AuthenticationTogglerOutlet { get; set; }

		[Outlet]
		AppKit.NSTextField BufferSize { get; set; }

		[Outlet]
		AppKit.NSButton ListenButtonOutlet { get; set; }

		[Outlet]
		AppKit.NSTextView MessageViewer { get; set; }

		[Action ("AuthenticationToggler:")]
		partial void AuthenticationToggler (AppKit.NSButton sender);

		[Action ("CacheToggler:")]
		partial void CacheToggler (AppKit.NSButton sender);

		[Action ("IdentityProtectionToggler:")]
		partial void IdentityProtectionToggler (AppKit.NSButton sender);

		[Action ("ImageFilterToggler:")]
		partial void ImageFilterToggler (AppKit.NSButton sender);

		[Action ("ListenButton:")]
		partial void ListenButton (AppKit.NSButton sender);

		[Action ("StreamingToggler:")]
		partial void StreamingToggler (AppKit.NSButton sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (BufferSize != null) {
				BufferSize.Dispose ();
				BufferSize = null;
			}

			if (ListenButtonOutlet != null) {
				ListenButtonOutlet.Dispose ();
				ListenButtonOutlet = null;
			}

			if (AuthenticationTogglerOutlet != null) {
				AuthenticationTogglerOutlet.Dispose ();
				AuthenticationTogglerOutlet = null;
			}

			if (MessageViewer != null) {
				MessageViewer.Dispose ();
				MessageViewer = null;
			}
		}
	}
}
