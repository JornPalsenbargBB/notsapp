﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;

namespace NotSApp
{
	public class ChatClient: ChatApplication
	{
		private TcpClient client;
		private string name;

		public ChatClient(string name, messageHandler updateUI): base(updateUI)
		{
			this.name = name;
		}

		/// <summary>
		/// Connect the specified hostname and onClose.
		/// </summary>
		/// <param name="hostname">Hostname.</param>
		/// <param name="onClose">On close.</param>
		public void connect(string hostname, onConnectionClose onClose)
		{
			this.client = new TcpClient(hostname,port);
			ThreadPool.QueueUserWorkItem(info => receiveData(client, onClose));
		}

		/// <summary>
		/// Sends a message to the server.
		/// </summary>
		/// <param name="message">Message.</param>
		public void sendMessage(string message)
		{
			message = message != "bye" ? name + " -> " + message : message;
			Byte[] byteArray = new Byte[message.Length];
			byteArray = Encoding.ASCII.GetBytes(message);
			client.GetStream().Write(byteArray, 0, byteArray.Length);
		}

	}
}
