﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Collections.Generic;
using System.Text;

namespace NotSApp
{
	public class ChatServer : ChatApplication
	{
		private TcpListener listener;

		private List<TcpClient> clients = new List<TcpClient>();

		public ChatServer(messageHandler updateUI): base(updateUI)
		{
			this.addMessageHandler(broadcastMessage);
		}

		/// <summary>
		/// Starts the server.
		/// </summary>
		public void startServer(Action<Exception> onException)
		{
			this.listener = new TcpListener(IPAddress.Any, port);
			ThreadPool.QueueUserWorkItem(info => {
				try
				{
					newClientListener();
				}
				catch (Exception e)
				{
					onException(e);
				}
			});
		}

		/// <summary>
		/// News the client listener.
		/// </summary>
		private void newClientListener()
		{
			listener.Start();
			updateUI("Listening for new clients..");

			// listen for new clients and if one connects, start a new thread with the receiveData method
			while (true)
			{
				TcpClient newClient = listener.AcceptTcpClient();
				updateUI("New client tries to connect!");
				clients.Add(newClient);
				ThreadPool.QueueUserWorkItem(info => receiveData(newClient, client => clients.Remove(client)));
			}
		}

		/// <summary>
		/// Broadcasts the message.
		/// </summary>
		/// <param name="message">Message.</param>
		private void broadcastMessage(string message)
		{
			clients.ForEach(client => {
				NetworkStream stream = client.GetStream();
				Byte[] byteArray = new Byte[message.Length];
				byteArray = Encoding.ASCII.GetBytes(message);
				stream.Write(byteArray, 0, byteArray.Length);
			});
		}

	}
}
