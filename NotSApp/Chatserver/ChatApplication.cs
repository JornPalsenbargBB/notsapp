﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Linq;

namespace NotSApp
{
	public abstract class ChatApplication
	{
		public delegate void messageHandler(string message);
		public delegate void onConnectionClose(TcpClient client);

		protected messageHandler updateUI;
		private List<messageHandler> messageHandlers = new List<messageHandler>();

		protected int port = 9000;
		protected int byteSize = 1024;

		public ChatApplication(messageHandler updateUI)
		{
			this.updateUI = updateUI;
		}

		/// <summary>
		/// Adds the message handler.
		/// </summary>
		/// <param name="messageHandler">Message handler.</param>
		public void addMessageHandler(messageHandler messageHandler)
		{
			this.messageHandlers.Add(messageHandler);
		}

		/// <summary>
		/// Fires the message handlers.
		/// </summary>
		/// <param name="message">Message.</param>
		protected void fireMessageHandlers(string message)
		{
			updateUI(message);
			messageHandlers.ForEach(messageHandler => {
				ThreadPool.QueueUserWorkItem(info => messageHandler(message));
			});
		}

		/// <summary>
		/// Receives the data.
		/// </summary>
		/// <param name="client">Client.</param>
		/// <param name="closeCB">Close cb.</param>
		protected void receiveData(TcpClient client, onConnectionClose closeCB)
		{
			string message = "";
			string newMessage = "";
			Byte[] byteArray = new Byte[byteSize];
			NetworkStream stream = client.GetStream();

			updateUI("Connected!");

			// Handle incoming messages
			while (true)
			{
				newMessage = "";

				do
				{
					Array.Clear(byteArray, 0, byteArray.Length);
					stream.Read(byteArray, 0, byteSize);
					int length = byteArray.TakeWhile(b => b != 0).Count();
					newMessage += Encoding.ASCII.GetString(byteArray, 0, length);
				} while (stream.DataAvailable);

				// if the message is bye, disconnect user.
				if (newMessage == "bye")
				{
					break;
				}

				message = newMessage;
				ThreadPool.QueueUserWorkItem(info => fireMessageHandlers(message));
			}

			byteArray = Encoding.ASCII.GetBytes("bye");
			stream.Write(byteArray, 0, byteArray.Length);
			stream.Close();
			client.Close();
			closeCB(client);
			updateUI("Connection closed!");
		}

		/// <summary>
		/// Sets the size of the buffer.
		/// </summary>
		/// <param name="newBufferSize">New buffer size.</param>
		public void setBufferSize(int newBufferSize)
		{
			byteSize = newBufferSize;
		}

	}
}
