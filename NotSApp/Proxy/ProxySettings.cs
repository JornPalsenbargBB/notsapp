﻿using System;

namespace NotSApp
{
    public class ProxySettings
    {
        public int Port = 3000;
        public int BufferSize = 1024;
        public bool CachingEnabled = false;
        public bool ImageFilterEnabled = false;
        public bool IdentityProtectionEnabled = false;
        public bool AuthenticationEnabled = false;
        public bool StreamingEnabled = false;
    }
}
