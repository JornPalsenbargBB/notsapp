﻿using System;
using System.Text;

namespace NotSApp
{
    public class ProxyAuthentication
    {
        string username = "admin";
        string password = "admin";

        public ProxyAuthentication()
        {}

        /// <summary>
        /// Checks if account is right.
        /// </summary>
        /// <returns><c>true</c>, if account is right, <c>false</c> otherwise.</returns>
        /// <param name="authHeader">Auth header.</param>
        public bool checkAccount(string authHeader) {
            bool accountIsRight = false;

            if (authHeader.Substring(0, 5).Equals("Basic"))
            {
                string basicString = authHeader.Substring(6, authHeader.Length - 6);
                byte[] data = Convert.FromBase64String(basicString);
                string decodedString = Encoding.UTF8.GetString(data);
                if (decodedString == username + ":" + password)
                {
                    accountIsRight = true;
                }
            }

            return accountIsRight;
        }

        /// <summary>
        /// Method to get a 401 unauthorized http response.
        /// </summary>
        /// <returns>401 Unauthorized http message.</returns>
        public HttpResponseMessage getWrongAuthResponseMessage() {
            HttpResponseMessage message = new HttpResponseMessage();

            message.HttpVersion = "HTTP/1.1";
            message.StatusCode = "401";
            message.ReasonPhrase = "Unauthorized";

            message.SetHeader("WWW-Authenticate", "Basic realm=\"Basic Auth enabled\"");

            return message;
        }
    }
}
