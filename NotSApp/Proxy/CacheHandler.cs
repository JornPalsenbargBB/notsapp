﻿using System;
using System.Collections.Generic;
using System.Timers;
using System.Threading.Tasks;

namespace NotSApp
{
    public class CacheHandler
    {
        private Dictionary<String, HttpResponseMessage> cacheContainer = new Dictionary<string, HttpResponseMessage>();
        private getResponseFromServer responseGetter;
        private Action<string> logger;

        public delegate HttpResponseMessage getResponseFromServer(HttpRequestMessage request);

        /// <summary>
        /// Initializes a new instance of the <see cref="T:NotSApp.CacheHandler"/> class.
        /// </summary>
        /// <param name="responseGetter">Method that handles getting a response from the server.</param>
        /// <param name="logger">Method that logs string</param>
        public CacheHandler(getResponseFromServer responseGetter, Action<string> logger)
        {
            this.responseGetter = responseGetter;
            this.logger = logger;
        }

        /// <summary>
        /// Caches the response.
        /// </summary>
        /// <param name="request">Request.</param>
        /// <param name="response">Response.</param>
        public void cacheResponse(HttpRequestMessage request, HttpResponseMessage response)
        {
            if (responseShouldBeCached(response))
            {
                logger("Adding " + request.GetStartLineString() + " to cache.");
                cacheContainer[request.GetStartLineString()] = response;
            }
        }

        /// <summary>
        /// Checks if response is cached
        /// </summary>
        /// <returns><c>true</c>, if response is cached, <c>false</c> if not.</returns>
        /// <param name="request">Request.</param>
        public bool responseIsCached(HttpRequestMessage request) {
            return cacheContainer.ContainsKey(request.GetStartLineString());
        }

        /// <summary>
        /// Checks if cached response is still valid
        /// </summary>
        /// <returns><c>true</c>, if cached response is still valid, <c>false</c> if not.</returns>
        /// <param name="request">Request.</param>
        public bool cachedResponseIsStillValid(HttpRequestMessage request) {
            try {
                HttpResponseMessage cachedResponse = cacheContainer[request.GetStartLineString()];

                if (cachedResponse.HasHeader("Expires") 
                    && getCurrentUnixTimestamp() > DateTimeToUnixTimestamp(Convert.ToDateTime(cachedResponse.GetHeader("Expires")))) {
                    logger("Expires header states content is expired");
                    remove(request);
                    return false;
                } else  {
                    logger("Checking with server if content has changed.");
                    RequestMethod backup = request.RequestMethod;
                    request.RequestMethod = RequestMethod.HEAD;
                    HttpResponseMessage headResponse = responseGetter(request);
                    request.RequestMethod = backup;
                    if (cachedResponse.HasHeader("etag") 
                        && headResponse.HasHeader("etag")
                        && cachedResponse.GetHeader("etag") != headResponse.GetHeader("etag")) {
                        remove(request);
                        logger("Content on server has changed (based on etag).");
                        return false;
                    } else {
                        logger("Content on server has not changed (based on etag).");
                        return true;
                    }
                }

            } catch (Exception) {
                return false;
            }
        }

        /// <summary>
        /// Gets the cached response.
        /// </summary>
        /// <returns>The cached response.</returns>
        /// <param name="request">Request.</param>
        public HttpResponseMessage getCachedResponse(HttpRequestMessage request) {
            return cacheContainer[request.GetStartLineString()];
        }

        /// <summary>
        /// Resets cache.
        /// </summary>
        public void reset() {
            cacheContainer.Clear();
        }

        private bool responseShouldBeCached(HttpResponseMessage response) {
            bool responsedecider = true;

            if (response.HasHeader("Cache-control") 
                && (response.GetHeader("Cache-control").Contains("no-cache")
                    || response.GetHeader("Cache-control").Contains("no-store") 
                    || response.GetHeader("Cache-control").Contains("private"))) {
                logger("Server forbids caching of response. (based on Cache-control)");
                responsedecider = false;
            }

            if (response.StatusCode == "401") {
                responsedecider = false;
            }

            return responsedecider;
        }

        private void remove(HttpRequestMessage request) {
            cacheContainer.Remove(request.GetStartLineString());
        }

        private static long getCurrentUnixTimestamp() {
            return (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        }

        private static long DateTimeToUnixTimestamp(DateTime dateTime)
        {
            return (Int32)(TimeZoneInfo.ConvertTimeToUtc(dateTime) - new DateTime(1970, 1, 1)).TotalSeconds;
        }

    }
}
