﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Compression;

namespace NotSApp
{
	public class ProxyServer
	{
        public delegate void onConnectionClose(TcpClient client);
		private Action<string> UiUpdateHandler;
		private Action<Exception> ExceptionHandler;

        private HttpParser httpParser = new HttpParser();
        private ProxyAuthentication authenticator = new ProxyAuthentication();

        private CacheHandler cache;
        private TcpListener TcpListener;
        private ProxySettings settings;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:NotSApp.ProxyServer"/> class.
        /// </summary>
        /// <param name="UiUpdateHandler">User interface update handler.</param>
        /// <param name="ExceptionHandler">Exception handler.</param>
		public ProxyServer(Action<string> UiUpdateHandler, Action<Exception> ExceptionHandler)
		{
			this.UiUpdateHandler = UiUpdateHandler;
			this.ExceptionHandler = ExceptionHandler;

            CacheHandler.getResponseFromServer responseFetcher = retreiveResponseFromExternal;
            cache = new CacheHandler(responseFetcher, UiUpdateHandler);
		}

        /// <summary>
        /// Starts proxy
        /// </summary>
        /// <param name="settings">Proxy Settings.</param>
        public void Start(ProxySettings settings)
		{
            this.settings = settings;

            TcpListener = new TcpListener(IPAddress.Any, settings.Port);
			ThreadPool.QueueUserWorkItem(info =>
			{
				try
				{
					HandleIncoming();
				}
				catch (Exception e)
				{
					ExceptionHandler(e);
				}
			});
		}

        /// <summary>
        /// Stop proxy
        /// </summary>
        public void Stop() {
            TcpListener.Stop();
            cache.reset();
            UiUpdateHandler("Proxy stopped");
        }

        /// <summary>
        /// handles incoming connection
        /// </summary>
		public void HandleIncoming()
		{
			TcpListener.Start();
            UiUpdateHandler("Proxy started on port " + settings.Port.ToString());

			while (true)
			{
				TcpClient newClient = TcpListener.AcceptTcpClient();
                ThreadPool.QueueUserWorkItem(info => receiveRequest(newClient));
			}
		}

        /// <summary>
        /// Handles incoming data
        /// </summary>
        /// <param name="client">Client.</param>
        protected void receiveRequest(TcpClient client)
        {
            HttpRequestMessage request = retreiveRequestFromStream(client.GetStream());
            //request.SetHeader("keep-alive", "close");
            //request.SetHeader("Connection", "close");

            UiUpdateHandler(request.RequestMethod.ToString() + " " + request.RequestUri);

            try {
                if (settings.StreamingEnabled)
                {
                    UiUpdateHandler("Streaming enabled, streaming response");
                    handleStreaming(request, client);
                }
                else
                {
                    handleNormal(request, client);
                }
            } catch (Exception e) {
                UiUpdateHandler("Error: " + e.Message);
            }

            client.GetStream().Close();
            client.Close();
        }

        private void handleNormal(HttpRequestMessage request, TcpClient client) {
            if (settings.IdentityProtectionEnabled && request.HasHeader("user-agent"))
            {
                request.removeHeader("user-agent");
                UiUpdateHandler("Identity protection enabled, user-agent header removed.");
            }

            HttpResponseMessage response;

            if (settings.AuthenticationEnabled)
            {
                response = getResponseIfAuthenticationEnabled(request);
            }
            else
            {
                response = tryToGetFromCacheOtherwiseRetreiveFromHost(request);
            }

            if (settings.CachingEnabled)
            {
                cache.cacheResponse(request, response);
            }

            UiUpdateHandler(response.StatusCode + " " + response.ReasonPhrase);

            if (settings.IdentityProtectionEnabled && response.HasHeader("server"))
            {
                response.removeHeader("server");
                UiUpdateHandler("Identity protection enabled, server header removed.");
            }

            Byte[] resBytes = handleImageFiltration(response);
            client.GetStream().Write(resBytes, 0, resBytes.Length);
        }

        private void handleStreaming(HttpRequestMessage request, TcpClient client) {
            
        }

        private HttpRequestMessage retreiveRequestFromStream(NetworkStream stream) {
            Byte[] byteArray = new Byte[settings.BufferSize];
            Byte[] byteMessage = new Byte[0];

            do
            {
                Array.Clear(byteArray, 0, byteArray.Length);
                stream.Read(byteArray, 0, settings.BufferSize);
                int length = byteArray.TakeWhile(b => b != 0).Count();
                Array.Resize(ref byteArray, length);
                Array.Resize(ref byteMessage, byteMessage.Length + length);
                byteArray.CopyTo(byteMessage, byteMessage.Length - length);
            } while (stream.DataAvailable);

            return httpParser.ByteArrayToHttpRequest(byteMessage);
        }

        private HttpResponseMessage tryToGetFromCacheOtherwiseRetreiveFromHost(HttpRequestMessage request) {
            if (settings.CachingEnabled && cache.responseIsCached(request) && cache.cachedResponseIsStillValid(request)) {
                UiUpdateHandler("retreiving " + request.GetStartLineString() + " from cache.");
                return cache.getCachedResponse(request);
            } else {
                UiUpdateHandler("retreiving " + request.GetStartLineString() + " from host.");
                return retreiveResponseFromExternal(request);
            }
        }

        private HttpResponseMessage retreiveResponseFromExternal(HttpRequestMessage request) {
            Uri requestUri = new Uri(request.RequestUri);
            TcpClient external = new TcpClient(requestUri.Host, requestUri.Port == -1 ? 80 : requestUri.Port);
            NetworkStream extStream = external.GetStream();
            extStream.Write(request.GetByteArray(), 0, request.GetByteArray().Length);
            var memory = new MemoryStream();
            extStream.CopyTo(memory);
            memory.Position = 0;
            return httpParser.ByteArrayToHttpResponse(memory.ToArray());
        }

        private HttpResponseMessage getResponseIfAuthenticationEnabled(HttpRequestMessage request) {
            try {
                if (!request.HasHeader("Authorization") || !authenticator.checkAccount(request.GetHeader("Authorization"))) {
                    UiUpdateHandler("User not correctly authenticated, sending 401 Not Authorized response.");
                    return authenticator.getWrongAuthResponseMessage();
                } else {
                    return tryToGetFromCacheOtherwiseRetreiveFromHost(request);
                }
            } catch (Exception) {
                return tryToGetFromCacheOtherwiseRetreiveFromHost(request);
            }
        }

        private Byte[] handleImageFiltration(HttpResponseMessage response) {
            try {
                if (settings.ImageFilterEnabled && response.GetHeader("content-type").Contains("image")) {
                    UiUpdateHandler("Image detected, replacing image with placeholder.");
                    var webclient = new WebClient();
                    var placeholderImageUri = "http://bit.ly/2mFCWdm";
                    return webclient.DownloadData(placeholderImageUri);
                } else {
                    return response.GetByteArray();
                }
            }
            catch (Exception) {
                return response.GetByteArray();
            }
        }
	}
}
